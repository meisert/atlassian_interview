package marketplaceService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.atlassian.interview.account.Application;
import com.atlassian.interview.account.models.Account;
import com.atlassian.interview.account.models.Contact;
import com.atlassian.interview.account.repositories.AccountRepository;
import com.atlassian.interview.account.repositories.ContactRepository;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class MarketplaceServiceTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private ContactRepository contactRepository;

    private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));

    private List<Account> testAccounts = new ArrayList();
    private List<Contact> testContacts = new ArrayList();

    @Before
    public void setup() throws Exception {
        Account account = new Account("e3460944-b4e4-4423-aa41-9df554091a16", "test comp1");
        account = accountRepository.save(account);
        testAccounts.add(account);
        Contact contact = new Contact("e3460944-b4e4-4423-aa41-9df554091a18", "cont2", account.getId());
        contact = contactRepository.save(contact);
        testContacts.add(contact);
    }

    @Test
    public void getAccount() throws Exception {
        this.mockMvc.perform(get("/accounts/" + testAccounts.get(0).getId())).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(this.testAccounts.get(0).getId()));
    }

    @Test
    public void postAccount() throws Exception {
        Account newAccount = new Account("e3460944-b4e4-4423-aa41-9df554091a17", "test comp3");
        String json = new ObjectMapper().writeValueAsString(newAccount);
        this.mockMvc.perform(post("/accounts/").contentType(contentType).content(json)).andExpect(status().isOk())
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$.id").value(newAccount.getId()));
    }

    @Test
    public void putAccount() throws Exception {
        Account updateAccount = new Account(testAccounts.get(0).getId(), "test comp2");
        String json = new ObjectMapper().writeValueAsString(updateAccount);
        this.mockMvc.perform(put("/accounts/" + testAccounts.get(0).getId()).contentType(contentType).content(json))
                .andExpect(status().isOk()).andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(testAccounts.get(0).getId()))
                .andExpect(jsonPath("$.companyName").value(updateAccount.getCompanyName()));
    }

    @Test
    public void getContactsForAccount() throws Exception {
        this.mockMvc.perform(get("/accounts/" + testContacts.get(0).getId() + "/contacts")).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(this.testContacts.get(0).getId()));
    }

    @Test
    public void getContact() throws Exception {
        this.mockMvc.perform(get("/contacts/" + testContacts.get(0).getId())).andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.id").value(this.testContacts.get(0).getId()));
    }

    @Test
    public void postContact() throws Exception {
        Contact newContact = new Contact("e3460944-b4e4-4423-aa41-9df554091a19", "contact4",
                testAccounts.get(0).getId());
        String json = new ObjectMapper().writeValueAsString(newContact);
        this.mockMvc.perform(post("/contacts/").contentType(contentType).content(json)).andExpect(status().isOk())
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$.id").value(newContact.getId()));
    }

    @Test
    public void putContact() throws Exception {
        Contact updatedContact = new Contact(testContacts.get(0).getId(), "contact5", testAccounts.get(0).getId());
        String json = new ObjectMapper().writeValueAsString(updatedContact);
        this.mockMvc.perform(post("/contacts/").contentType(contentType).content(json)).andExpect(status().isOk())
                .andExpect(content().contentType(contentType)).andExpect(jsonPath("$.id").value(updatedContact.getId()))
                .andExpect(jsonPath("$.name").value(updatedContact.getName()));
    }

}
