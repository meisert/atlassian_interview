package com.atlassian.interview.account.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.interview.account.models.Contact;
import com.atlassian.interview.account.service.ContactService;

@RestController
@RequestMapping("/contacts")
public class ContactController {
    private static Logger log = LoggerFactory.getLogger(ContactController.class);
    @Autowired
    ContactService contactService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<Contact> getContact(@PathVariable("id") String id) {
        log.debug("GET /contact/{id} {}", id);
        Contact contact = new Contact();
        try {
            contact = contactService.getContact(id);
            return new ResponseEntity<Contact>(contact, HttpStatus.OK);

        } catch (Exception exc) {
            log.error("Error retrieving contact: ", exc);
            return new ResponseEntity<Contact>(contact, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Contact> createContact(@RequestBody Contact contact) {
        Contact returnContact = new Contact();
        try {
            returnContact = contactService.saveContact(contact);
            return new ResponseEntity<Contact>(returnContact, HttpStatus.OK);
        } catch (Exception exc) {
            log.error("Error saving contact: ", exc);
            return new ResponseEntity<Contact>(returnContact, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Contact> Contact(@PathVariable("id") String id, @RequestBody Contact contact) {
        Contact returnContact = null;
        try {
            returnContact = contactService.getContact(id);
            if (contact == null) {
                return new ResponseEntity<Contact>(returnContact, HttpStatus.BAD_REQUEST);
            }
            contact = contactService.saveContact(contact);
            return new ResponseEntity<Contact>(contact, HttpStatus.OK);
        } catch (Exception exc) {
            log.error("Error saving contact: ", exc);
            return new ResponseEntity<Contact>(contact, HttpStatus.BAD_REQUEST);
        }
    }

}
