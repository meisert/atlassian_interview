package com.atlassian.interview.account.controllers;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.interview.account.models.Account;
import com.atlassian.interview.account.models.Contact;
import com.atlassian.interview.account.service.AccountService;
import com.atlassian.interview.account.service.ContactService;

@RestController
@RequestMapping("/accounts")
public class AccountController {
    private static Logger log = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    AccountService accountService;

    @Autowired
    ContactService contactService;

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<Account> getAccount(@PathVariable("id") String id) {
        log.debug("GET /account/{id} {}", id);
        Account account = new Account();
        try {
            account = accountService.getAccount(id);
            if (account != null) {
                return new ResponseEntity<Account>(account, HttpStatus.OK);
            } else {
                account = null;
                return new ResponseEntity<Account>(account, HttpStatus.OK);
            }

        } catch (Exception exc) {
            log.error("Error retrieving account: ", exc);
            return new ResponseEntity<Account>(account, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/{id}/contacts", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ResponseBody
    public ResponseEntity<List<Contact>> getContacts(@PathVariable("id") String id) {
        log.debug("GET /account/{id} {}", id);
        List<Contact> contacts = new ArrayList();
        try {
            contacts = contactService.getContacts(id);
            if (contacts != null && !contacts.isEmpty()) {
                return new ResponseEntity<List<Contact>>(contacts, HttpStatus.OK);
            } else {
                return new ResponseEntity<List<Contact>>(contacts, HttpStatus.OK);
            }

        } catch (Exception exc) {
            log.error("Error retrieving account: ", exc);
            return new ResponseEntity<List<Contact>>(contacts, HttpStatus.BAD_REQUEST);
        }

    }

    @RequestMapping(value = "/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        Account returnAccount = new Account();
        try {
            returnAccount = accountService.saveAccount(account);
            return new ResponseEntity<Account>(returnAccount, HttpStatus.OK);
        } catch (Exception exc) {
            log.error("Error saving account: ", exc);
            return new ResponseEntity<Account>(returnAccount, HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_UTF8_VALUE,
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<Account> Account(@PathVariable("id") String id, @RequestBody Account account) {
        Account returnAccount = null;
        try {
            returnAccount = accountService.getAccount(id);
            if (account == null) {
                return new ResponseEntity<Account>(returnAccount, HttpStatus.BAD_REQUEST);
            }
            account = accountService.saveAccount(account);
            return new ResponseEntity<Account>(account, HttpStatus.OK);
        } catch (Exception exc) {
            log.error("Error saving account: ", exc);
            return new ResponseEntity<Account>(account, HttpStatus.BAD_REQUEST);
        }
    }

}
