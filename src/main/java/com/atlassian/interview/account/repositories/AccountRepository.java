package com.atlassian.interview.account.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.atlassian.interview.account.models.Account;

public interface AccountRepository extends JpaRepository<Account, String>, JpaSpecificationExecutor<Account> {
}
