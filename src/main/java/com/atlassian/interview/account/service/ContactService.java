package com.atlassian.interview.account.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.interview.account.models.Contact;
import com.atlassian.interview.account.repositories.ContactRepository;

@Service
public class ContactService {

    @Autowired
    ContactRepository contactRepository;

    public Contact getContact(String id) {
        Optional<Contact> contanctOption = contactRepository.findById(id);
        if (contanctOption.isPresent()) {
            return contanctOption.get();
        } else {
            return null;
        }
    }

    public List<Contact> getContacts(String accountId) {
        return contactRepository.findByAccountId(accountId);
    }

    public Contact saveContact(Contact contact) {
        return contactRepository.save(contact);
    }

}
