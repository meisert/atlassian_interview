package com.atlassian.interview.account.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.atlassian.interview.account.models.Account;
import com.atlassian.interview.account.repositories.AccountRepository;

@Service
public class AccountService {

    @Autowired
    AccountRepository accountRepository;

    public Account getAccount(String id) {
        Optional<Account> accountOption = accountRepository.findById(id);
        if (accountOption.isPresent()) {
            return accountOption.get();
        } else {
            return null;
        }
    }

    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

}
