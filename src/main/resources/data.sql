insert into account (ID, COMPANY_NAME) 
values('e3460944-b4e4-4423-aa41-9df554091a14', 'Atlassian');

insert into account (ID, COMPANY_NAME) 
values('a9a48050-d653-490e-b94b-257b5893ecbb', 'Amazon');

insert into contact (ID, NAME, ACCOUNT_ID) 
values('6ced613e-d480-4717-b6a8-51de9775bdd2', 'John Doe', 'e3460944-b4e4-4423-aa41-9df554091a14');

insert into contact (ID, NAME, ACCOUNT_ID) 
values('6ced613e-d480-4717-b6a8-51de9775bdd3', 'Jane Doe', 'e3460944-b4e4-4423-aa41-9df554091a14');
