# atlassian_interview

this can be built by maven commands
Java project that uses Spring Boot libraries

To build run (need to install maven or IDE like Eclipse)
mvn clean verify

If you can't build this at command line then just run the JAR file directly that's included.  Eclipse works best for building.

Then run this as a Java application at command line like
java -jar target/marketplaceService-0.0.1-SNAPSHOT.jar

Or run in eclipse IDE running the Java Application.java file with main method in it.

Can call this service from POSTMAN for example with this command:
localhost:4567/accounts/e3460944-b4e4-4423-aa41-9df554091a14/contacts

The application in current state will run in dev mode using an in memory DB H2.

